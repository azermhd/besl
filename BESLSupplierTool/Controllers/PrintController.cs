﻿using BESLSupplierTool.Models.ReturnModels;
using System;
using System.IO;
using System.Web;
using System.Web.Mvc;

namespace BESLSupplierTool.Controllers
{
    public class PrintController : Controller
    {
        public FileResult DownloadFile(string directory, string fileName, string fileDownloadName)
        {
            byte[] fileBytes = System.IO.File.ReadAllBytes(directory + "\\" + fileName);
            return File(fileBytes, System.Net.Mime.MediaTypeNames.Application.Octet, fileDownloadName);
        }

        [HttpPost]
        public ActionResult UploadInquiryExcel()
        {
            // Checking no of files injected in Request object  
            if (Request.Files.Count > 0)
            {
                try
                {
                    //  Get all files from Request object  
                    HttpFileCollectionBase files = Request.Files;
                    for (int i = 0; i < files.Count; i++)
                    {
                        HttpPostedFileBase file = files[i];
                        string fname;

                        // Checking for Internet Explorer  
                        if (Request.Browser.Browser.ToUpper() == "IE" || Request.Browser.Browser.ToUpper() == "INTERNETEXPLORER")
                        {
                            string[] testfiles = file.FileName.Split(new char[] { '\\' });
                            fname = testfiles[testfiles.Length - 1];
                        }
                        else
                        {
                            fname = file.FileName;
                        }

                        // Get the complete folder path and store the file inside it.  
                        fname = Path.Combine(Server.MapPath("~/Content/Docs/Inquiries/"), "fname.xlsx");
                        file.SaveAs(fname);
                    }

                    JsonResultMainReturn retrun = new JsonResultMainReturn(true, "success");
                    return Json(new { JsonResultMainReturn = retrun, FileName = "fname.xlsx" });
                }
                catch (Exception ex)
                {
                    JsonResultMainReturn retrun = new JsonResultMainReturn(false, ex.Message);
                    return Json(new { JsonResultMainReturn = retrun });
                }
            }
            else
            {
                JsonResultMainReturn retrun = new JsonResultMainReturn(false, "No files selected");
                return Json(new { JsonResultMainReturn = retrun });
            }
        }

        public FileResult DownloadPdf()
        {
            string fileName = "";
            string folderName = "TimeSheetDaily";
            string subPath = Server.MapPath("~/Content/Docs") + "\\" + folderName;
            byte[] fileBytes = System.IO.File.ReadAllBytes(subPath + "\\" + fileName);
            return File(fileBytes, System.Net.Mime.MediaTypeNames.Application.Octet, fileName);
        }

        public JsonResult CreatePdf(DateTime? currentDate, int? DepartmentId, int? EmployeeListType)
        {
            try
            {
                JsonResultMainReturn retrun = new JsonResultMainReturn(true, "succes");
                return Json(retrun);
            }
            catch (Exception ex)
            {
                JsonResultMainReturn retrun = new JsonResultMainReturn(false, "false");
                return Json(retrun);
            }
        }

        public FileResult DownloadExcel()
        {
            string fileName = "";
            string folderName = "TimeSheetDaily";
            string subPath = Server.MapPath("~/Content/Docs") + "\\" + folderName;
            byte[] fileBytes = System.IO.File.ReadAllBytes(subPath + "\\" + fileName);
            return File(fileBytes, System.Net.Mime.MediaTypeNames.Application.Octet, fileName);
        }

        public JsonResult CreateExcel()
        {
            try
            {
                JsonResultMainReturn retrun = new JsonResultMainReturn(true, "succes");
                return Json(retrun);
            }
            catch (Exception ex)
            {
                JsonResultMainReturn retrun = new JsonResultMainReturn(false, "false");
                return Json(retrun);
            }
        }
    }
}