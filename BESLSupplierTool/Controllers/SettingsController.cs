﻿using BESLSupplierTool.Models.DBModels;
using Kendo.Mvc.UI;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Kendo.Mvc.Extensions;

namespace BESLSupplierTool.Controllers
{
    public class SettingsController : Controller
    {
        DatabaseContext db = new DatabaseContext();
        // GET: Settings
        public ActionResult Index()
        {
            return View();
        }

        #region CompanyDetailsGridCrud

        public ActionResult CompanyDetailsRead([DataSourceRequest]DataSourceRequest request)
        {
            return Json(db.CompanyDetails.ToDataSourceResult(request));
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult CompanyDetailsCreate([DataSourceRequest]DataSourceRequest request, CompanyDetails companyDetails)
        {
            if (ModelState.IsValid)
            {
                companyDetails.CompanyName = companyDetails.CompanyName.Trim();
                companyDetails.OfferPrefix = companyDetails.OfferPrefix.Trim();
                db.CompanyDetails.Add(companyDetails);
                db.SaveChanges();
            }
            return Json(new[] { companyDetails }.ToDataSourceResult(request, ModelState));
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult CompanyDetailsUpdate([DataSourceRequest]DataSourceRequest request, CompanyDetails companyDetails)
        {
            if (ModelState.IsValid)
            {
                CompanyDetails companyDetailsInDb = db.CompanyDetails.Find(companyDetails.CompanyId);
                companyDetailsInDb.CompanyName = companyDetails.CompanyName.Trim();
                companyDetailsInDb.OfferPrefix = companyDetails.OfferPrefix.Trim();
                companyDetailsInDb.OfferNoLength = companyDetails.OfferNoLength;
                db.SaveChanges();
            }
            return Json(new[] { companyDetails }.ToDataSourceResult(request, ModelState));
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult CompanyDetailsDelete([DataSourceRequest]DataSourceRequest request, CompanyDetails companyDetails)
        {
            if (ModelState.IsValid)
            {
                CompanyDetails compDet = db.CompanyDetails.FirstOrDefault(f => f.CompanyId == companyDetails.CompanyId);
                db.CompanyDetails.Remove(compDet);
                db.SaveChanges();
            }
            return Json(new[] { companyDetails }.ToDataSourceResult(request, ModelState));
        }
        #endregion
    }
}