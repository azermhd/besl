﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.Mvc;
using Kendo.Mvc.Extensions;
using Kendo.Mvc.UI;
using BESLSupplierTool.Models.DBModels;
using BESLSupplierTool.BusinessLayer.Utilities;
using BESLSupplierTool.Models.ReturnModels;
using BESLSupplierTool.Models.ViewModels;
using System.Globalization;
using BESLSupplierTool.BusinessLayer.Docs;

namespace BESLSupplierTool.Controllers
{
    public class InquiryController : Controller
    {
        DatabaseContext db;
        MainChecker mainChecker;
        public InquiryController()
        {
            db = new DatabaseContext();
            mainChecker = new MainChecker();
        }
        // GET: Inquiry
        public ActionResult Index()
        {
            return View();
        }

        public ActionResult PrintInquiry(int? InquiryId)
        {
            PrintInquiry model;
            if (InquiryId == null)
            {
                model = new PrintInquiry();
                return View(model);
            }
            Inquiry inquiry = db.Inquiry.AsNoTracking().FirstOrDefault(f => f.InquiryId == InquiryId.Value);
            InquiryItem[] itemsFromDb = db.InquiryItem.AsNoTracking().Where(w => w.InquiryId == inquiry.InquiryId).ToArray();
            InquiryItemView[] itemsView = new InquiryItemView[itemsFromDb.Length];
            for (int i = 0; i < itemsFromDb.Length; i++)
            {
                itemsView[i] = new InquiryItemView(itemsFromDb[i]);
            }
            model = new PrintInquiry
            {
                Date = DateTime.Now.ToString("dd.MM.yyyy"),
                InquiryNo = inquiry.InquiryNo,
                ItemList = itemsView,
                ProjectNo = inquiry.ProjectNo,
            };
            return View(model);
        }

        #region inquiryedittool

        public ActionResult EditInquiry(int InquiryId)
        {
            if (InquiryId == 0)
            {
                return RedirectToAction("Index");
            }
            Inquiry inquiry = db.Inquiry.AsNoTracking().FirstOrDefault(f => f.InquiryId == InquiryId);
            if (inquiry == null)
            {
                return RedirectToAction("Index");
            }

            InquiryItem[] inquiryItems = db.InquiryItem.AsNoTracking().Where(s => s.InquiryId == InquiryId).ToArray();
            InquiryItemView[] inquiryItemViews = new InquiryItemView[inquiryItems.Length];
            for (int i = 0; i < inquiryItems.Length; i++)
            {
                inquiryItemViews[i] = new InquiryItemView(inquiryItems[i]);
            }

            EditInquiryView model = new EditInquiryView
            {
                InquiryId = InquiryId,
                InquiryItems = inquiryItemViews,
                InquiryNo = inquiry.InquiryNo
            };
            return View(model);
        }

        public JsonResult TestInquiryItemsFromTool(InquiryItemRowInEditTool[] InquiryItems, int InquiryId)
        {
            try
            {
                InquiryItemView[] items = cleanInquiryItemsFromTool(InquiryItems, InquiryId);
                return Json(
                    new
                    {
                        JsonResultMainReturn = new JsonResultMainReturn(true, "Success"),
                        InquiryItems = items,
                    },
                    JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                return Json(new { JsonResultMainReturn = new JsonResultMainReturn(false, ex.Message) }, JsonRequestBehavior.AllowGet);
            }
        }

        public JsonResult SaveInquiryItemsFromTool(InquiryItemRowInEditTool[] InquiryItems, int InquiryId)
        {
            try
            {
                InquiryItemView[] items = cleanInquiryItemsFromTool(InquiryItems, InquiryId);
                InquiryItem[] itemsFromDb = db.InquiryItem.Where(w => w.InquiryId == InquiryId).ToArray();

                if (items.Length > itemsFromDb.Length)
                {
                    for (int i = 0; i < itemsFromDb.Length; i++)
                    {
                        copyInquiryItemViewToItem(items[i], itemsFromDb[i]);
                    }
                    for (int i = itemsFromDb.Length; i < items.Length; i++)
                    {
                        db.InquiryItem.Add(new InquiryItem(items[i]));
                    }
                }
                else if (items.Length == itemsFromDb.Length)
                {
                    for (int i = 0; i < itemsFromDb.Length; i++)
                    {
                        copyInquiryItemViewToItem(items[i], itemsFromDb[i]);
                    }
                }
                else if (items.Length < itemsFromDb.Length)
                {
                    for (int i = 0; i < items.Length; i++)
                    {
                        copyInquiryItemViewToItem(items[i], itemsFromDb[i]);
                    }
                    for (int i = items.Length; i < itemsFromDb.Length; i++)
                    {
                        db.InquiryItem.Remove(itemsFromDb[i]);
                    }
                }
                db.SaveChanges();

                return Json(
                    new
                    {
                        JsonResultMainReturn = new JsonResultMainReturn(true, "Success"),
                        InquiryItems = items,
                    },
                    JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                return Json(new { JsonResultMainReturn = new JsonResultMainReturn(false, ex.Message) }, JsonRequestBehavior.AllowGet);
            }
        }

        public JsonResult GetInquiryItemsFromFile(int InquiryId, string FileName)
        {
            try
            {
                InquiryItemView[] items = new ExcelWorksheets().readInquiryItemsFromExcel(FileName);
                return Json(
                    new
                    {
                        JsonResultMainReturn = new JsonResultMainReturn(true, "Success"),
                        InquiryItems = items,
                    },
                    JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                return Json(new { JsonResultMainReturn = new JsonResultMainReturn(false, ex.Message) }, JsonRequestBehavior.AllowGet);
            }
        }

        private InquiryItemView[] cleanInquiryItemsFromTool(InquiryItemRowInEditTool[] inquiryItemListFromTool, int InquiryId)
        {
            InquiryItemView[] inquiryItems = new InquiryItemView[inquiryItemListFromTool.Length];
            for (int i = 0; i < inquiryItemListFromTool.Length; i++)
            {
                var item = inquiryItemListFromTool[i];
                InquiryItemView sss = new InquiryItemView
                {
                    InquiryId = InquiryId,
                    Description = item.description == null ? " " : item.description,
                    Unit = item.unit == null ? " " : item.unit,
                };

                decimal val;
                if (item.quantity == null || !decimal.TryParse(item.quantity.Trim(new char[] { '.', ',', ' ' }).Replace(",", "."), NumberStyles.Number, CultureInfo.InvariantCulture, out val))
                {
                    val = 0M;
                }
                sss.Quantity = val;

                inquiryItems[i] = sss;
            }
            return inquiryItems;
        }

        #endregion

        #region createOfferForInquiry

        public JsonResult CreateOfferForInquiryNo(int? InquiryId, bool? OverrideOffer)
        {
            if (InquiryId == null)
            {
                return Json(new { Existence = false, JsonResultMainReturn = new JsonResultMainReturn(false, "InquiryId is null") }, JsonRequestBehavior.AllowGet);
            }
            else if (OverrideOffer == null)
            {
                return Json(new { Existence = false, JsonResultMainReturn = new JsonResultMainReturn(false, "overrideOffer is null") }, JsonRequestBehavior.AllowGet);
            }

            try
            {
                BeslOfferController offerCont = new BeslOfferController();
                BeslOffer offer = db.BeslOffer.AsNoTracking().FirstOrDefault(f => f.InquiryId == InquiryId.Value);
                if (offer != null && !OverrideOffer.Value)
                {
                    return Json(new JsonResultMainReturn(true, "Offer is exist and cannot be overrided"), JsonRequestBehavior.AllowGet);
                }
                Inquiry currentInquiry = db.Inquiry.AsNoTracking().FirstOrDefault(f => f.InquiryId == InquiryId.Value);
                InquiryItem[] currentInquiryItems = db.InquiryItem.AsNoTracking().Where(w => w.InquiryId == currentInquiry.InquiryId).ToArray();

                if (offer == null)
                {
                    BeslOffer newBeslOffer = new BeslOffer
                    {
                        Currency = "AZN",
                        DateOffer = DateTime.Now.Date,
                        InquiryId = InquiryId.Value,
                        InquiryNo = currentInquiry.InquiryNo,
                        QuoteNo = offerCont.getNewQuoteNo(),
                        VAT = 18,
                        TermsOfDelivery = "DDP Baku",
                        TermsOfPayment = "30 days / 30 gün"
                    };

                    db.BeslOffer.Add(newBeslOffer);
                    db.SaveChanges();

                    foreach (var item in currentInquiryItems)
                    {
                        BeslOfferItem newBeslOfferItem = new BeslOfferItem
                        {
                            BeslOfferId = newBeslOffer.BeslOfferId,
                            Description = item.Description,
                            DescriptionForSale = item.Description,
                            Quantity = item.Quantity,
                            Unit = item.Unit,
                        };
                        db.BeslOfferItem.Add(newBeslOfferItem);
                    }
                    db.SaveChanges();
                }
                else
                {
                    BeslOfferItem[] oldBeslOfferItems = db.BeslOfferItem.Where(w => w.BeslOfferId == offer.BeslOfferId).ToArray();
                    int inquiryItemCount = currentInquiryItems.Count();
                    int oldBeslOfferItemCount = oldBeslOfferItems.Count();

                    if (inquiryItemCount > oldBeslOfferItemCount)
                    {
                        for (int i = 0; i < oldBeslOfferItemCount; i++)
                        {
                            copyInquiryItemToOfferItem(oldBeslOfferItems[i], currentInquiryItems[i]);
                        }
                        for (int i = oldBeslOfferItemCount; i < inquiryItemCount; i++)
                        {
                            BeslOfferItem newBeslOfferItem = new BeslOfferItem
                            {
                                BeslOfferId = offer.BeslOfferId,
                                Description = currentInquiryItems[i].Description,
                                DescriptionForSale = currentInquiryItems[i].Description,
                                Quantity = currentInquiryItems[i].Quantity,
                                Unit = currentInquiryItems[i].Unit,
                            };
                            db.BeslOfferItem.Add(newBeslOfferItem);
                        }
                    }
                    else if (inquiryItemCount == oldBeslOfferItemCount)
                    {
                        for (int i = 0; i < oldBeslOfferItemCount; i++)
                        {
                            copyInquiryItemToOfferItem(oldBeslOfferItems[i], currentInquiryItems[i]);
                        }
                    }
                    else
                    {
                        for (int i = 0; i < inquiryItemCount; i++)
                        {
                            copyInquiryItemToOfferItem(oldBeslOfferItems[i], currentInquiryItems[i]);
                        }
                        for (int i = inquiryItemCount; i < oldBeslOfferItemCount; i++)
                        {
                            db.BeslOfferItem.Remove(oldBeslOfferItems[i]);
                        }
                    }
                    db.SaveChanges();
                }
                return Json(new JsonResultMainReturn(true, "Success"), JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                return Json(new JsonResultMainReturn(true, ex.Message), JsonRequestBehavior.AllowGet);
            }
        }

        public JsonResult CheckOfferExistenceForInquiryNo(int? InquiryId)
        {
            try
            {
                BeslOffer offer = db.BeslOffer.AsNoTracking().FirstOrDefault(f => f.InquiryId == InquiryId.Value);
                if (offer != null)
                {
                    return Json(new { Existence = true, JsonResultMainReturn = new JsonResultMainReturn(true, "") }, JsonRequestBehavior.AllowGet);
                }
                else
                {
                    return Json(new { Existence = false, JsonResultMainReturn = new JsonResultMainReturn(true, "") }, JsonRequestBehavior.AllowGet);
                }
            }
            catch (Exception ex)
            {
                return Json(new { Existence = false, JsonResultMainReturn = new JsonResultMainReturn(true, ex.Message) }, JsonRequestBehavior.AllowGet);
            }
        }

        private void clearOfferItem(BeslOfferItem item)
        {
            item.Amount = 0;
            item.Comments = "";
            item.DeliveryDuration = "";
            item.Description = "";
            item.DescriptionForSale = "";
            item.Quantity = 0;
            item.Unit = "";
            item.UnitPrice = 0;
        }

        private void copyInquiryItemToOfferItem(BeslOfferItem offeritem, InquiryItem inquiryItem)
        {
            offeritem.Description = inquiryItem.Description;
            offeritem.DescriptionForSale = inquiryItem.Description;
            offeritem.Quantity = inquiryItem.Quantity;
            offeritem.Unit = inquiryItem.Unit;
        }

        private void copyInquiryItemViewToItem(InquiryItemView inquiryItemView, InquiryItem inquiryItem)
        {
            inquiryItem.InquiryId = inquiryItemView.InquiryId;
            inquiryItem.Description = inquiryItemView.Description;
            inquiryItem.Quantity = inquiryItemView.Quantity;
            inquiryItem.Unit = inquiryItemView.Unit;
        }

        #endregion

        #region Inquiry

        public ActionResult InquiryRead([DataSourceRequest]DataSourceRequest request)
        {
            return Json(db.Inquiry.ToDataSourceResult(request));
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult InquiryCreate([DataSourceRequest]DataSourceRequest request, Inquiry inquiry)
        {
            string check = mainChecker.check(inquiry);
            if (check.CompareTo("") != 0 && check != null)
            {
                ModelState.AddModelError(String.Empty, check);
            }
            if (ModelState.IsValid)
            {
                inquiry.InquiryNo = inquiry.InquiryNo.Trim();
                db.Inquiry.Add(inquiry);
                db.SaveChanges();
            }
            return Json(new[] { inquiry }.ToDataSourceResult(request, ModelState));
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult InquiryUpdate([DataSourceRequest]DataSourceRequest request, Inquiry inquiry)
        {
            string check = mainChecker.check(inquiry);
            if (check.CompareTo("") != 0 && check != null)
            {
                ModelState.AddModelError(String.Empty, check);
            }
            if (ModelState.IsValid)
            {
                Inquiry inquiryInDb = db.Inquiry.Find(inquiry.InquiryId);
                inquiryInDb.InquiryNo = inquiry.InquiryNo.Trim();
                inquiryInDb.Attn = inquiry.Attn;
                inquiryInDb.Buyer = inquiry.Buyer;
                inquiryInDb.ProjectNo = inquiry.ProjectNo;
                inquiryInDb.To = inquiry.To;
                db.SaveChanges();
            }
            return Json(new[] { inquiry }.ToDataSourceResult(request, ModelState));
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult InquiryDelete([DataSourceRequest]DataSourceRequest request, Inquiry inquiry)
        {
            if (ModelState.IsValid)
            {
                Inquiry inqu = db.Inquiry.FirstOrDefault(f => f.InquiryId == inquiry.InquiryId);
                if (inqu != null)
                {
                    List<InquiryItem> itemList = db.InquiryItem.Where(w => w.InquiryId == inquiry.InquiryId).ToList();

                    for (int i = 0; i < itemList.Count; i++)
                    {
                        db.InquiryItem.Remove(itemList[i]);
                    }
                    db.Inquiry.Remove(inqu);
                    db.SaveChanges();
                }
            }
            return Json(new[] { inquiry }.ToDataSourceResult(request, ModelState));
        }
        #endregion

        #region InquiryItems
        public ActionResult InquiryItemRead([DataSourceRequest]DataSourceRequest request, int id)
        {
            InquiryItem[] inquiryItems = db.InquiryItem.AsNoTracking().Where(w => w.InquiryId == id).ToArray();
            int itemCount = inquiryItems.Count();
            InquiryItemView[] inquiryItemsView = new InquiryItemView[itemCount];
            for (int i = 0; i < itemCount; i++)
            {
                inquiryItemsView[i] = new InquiryItemView(inquiryItems[i]);
            }
            return Json(inquiryItemsView.ToDataSourceResult(request));
        }

        //[AcceptVerbs(HttpVerbs.Post)]
        //public ActionResult InquiryItemCreate([DataSourceRequest]DataSourceRequest request, InquiryItem inquiryItem, int id)
        //{
        //    if (ModelState.IsValid)
        //    {
        //        inquiryItem.InquiryId = id;
        //        db.InquiryItem.Add(inquiryItem);
        //        db.SaveChanges();
        //    }

        //    return Json(new[] { inquiryItem }.ToDataSourceResult(request, ModelState));
        //}

        public ActionResult InquiryItemCreate([DataSourceRequest]DataSourceRequest request, [Bind(Prefix = "models")]IEnumerable<InquiryItemView> inquiryItemsView, int id)
        {
            if (ModelState.IsValid)
            {
                foreach (var item in inquiryItemsView)
                {
                    item.InquiryId = id;
                    db.InquiryItem.Add(new InquiryItem(item));
                }
                db.SaveChanges();
            }
            return Json(inquiryItemsView.ToDataSourceResult(request, ModelState));
        }

        //[AcceptVerbs(HttpVerbs.Post)]
        //public ActionResult InquiryItemUpdate([DataSourceRequest]DataSourceRequest request, InquiryItem inquiryItem)
        //{
        //    if (ModelState.IsValid)
        //    {
        //        InquiryItem inquiryItemInDb = db.InquiryItem.FirstOrDefault(f => f.InquiryItemId == inquiryItem.InquiryItemId);
        //        inquiryItemInDb.Description = inquiryItem.Description;
        //        inquiryItemInDb.InquiryId = inquiryItem.InquiryId;
        //        inquiryItemInDb.Quantity = inquiryItem.Quantity;
        //        inquiryItemInDb.Unit = inquiryItem.Unit;
        //        db.SaveChanges();
        //    }

        //    return Json(new[] { inquiryItem }.ToDataSourceResult(request, ModelState));
        //}

        public ActionResult InquiryItemUpdate([DataSourceRequest]DataSourceRequest request, [Bind(Prefix = "models")]IEnumerable<InquiryItemView> inquiryItemsView)
        {
            if (ModelState.IsValid)
            {
                foreach (var item in inquiryItemsView)
                {
                    InquiryItem inquiryItemInDb = db.InquiryItem.FirstOrDefault(f => f.InquiryItemId == item.InquiryItemId);
                    inquiryItemInDb.Description = item.Description;
                    inquiryItemInDb.InquiryId = item.InquiryId;
                    inquiryItemInDb.Quantity = item.Quantity;
                    inquiryItemInDb.Unit = item.Unit;
                }
                db.SaveChanges();
            }
            return Json(inquiryItemsView.ToDataSourceResult(request, ModelState));
        }

        //[AcceptVerbs(HttpVerbs.Post)]
        public ActionResult InquiryItemDelete([DataSourceRequest]DataSourceRequest request, [Bind(Prefix = "models")]IEnumerable<InquiryItemView> inquiryItemsView)
        {
            if (ModelState.IsValid)
            {
                foreach (var item in inquiryItemsView)
                {
                    InquiryItem inqu = db.InquiryItem.FirstOrDefault(f => f.InquiryId == item.InquiryId);
                    if (inqu != null)
                    {
                        db.InquiryItem.Remove(inqu);
                    }
                }
                db.SaveChanges();
            }
            return Json(new[] { inquiryItemsView }.ToDataSourceResult(request, ModelState));
        }
        #endregion
    }
}