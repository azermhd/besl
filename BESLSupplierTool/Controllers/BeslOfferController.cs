﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Kendo.Mvc.Extensions;
using Kendo.Mvc.UI;
using BESLSupplierTool.Models.DBModels;
using BESLSupplierTool.Models.ViewModels;
using System.Globalization;
using System.Text.RegularExpressions;
using BESLSupplierTool.Models.ReturnModels;
using BESLSupplierTool.BusinessLayer.Docs;

namespace BESLSupplierTool.Controllers
{
    public class BeslOfferController : Controller
    {
        DatabaseContext db;
        public BeslOfferController()
        {
            db = new DatabaseContext();
        }
        // GET: BeslOffer
        public ActionResult Index()
        {
            return View();
        }

        public ActionResult PrintOffer(int? BeslOfferId)
        {
            PrintOffer model;
            if (BeslOfferId == null)
            {
                model = new PrintOffer();
                return View(model);
            }
            model = getPrintOfferData(BeslOfferId.Value);
            return View(model);
        }

        public PrintOffer getPrintOfferData(int BeslOfferId)
        {
            PrintOffer model;
            BeslOffer offer = db.BeslOffer.AsNoTracking().FirstOrDefault(w => w.BeslOfferId == BeslOfferId);
            Inquiry inquiry = db.Inquiry.AsNoTracking().FirstOrDefault(f => f.InquiryId == offer.InquiryId);
            BeslOfferItem[] itemsFromDb = db.BeslOfferItem.AsNoTracking().Where(w => w.BeslOfferId == offer.BeslOfferId).ToArray();
            BeslOfferItemView[] itemsView = new BeslOfferItemView[itemsFromDb.Length];
            for (int i = 0; i < itemsFromDb.Length; i++)
            {
                itemsView[i] = new BeslOfferItemView(itemsFromDb[i]);
            }

            model = new PrintOffer
            {
                Attn = inquiry.Attn,
                Buyer = inquiry.Buyer,
                Currency = offer.Currency,
                Date = offer.DateOffer.ToString("dd.MM.yyyy"),
                InquiryNo = inquiry.InquiryNo,
                ItemList = itemsView,
                ProjectNo = inquiry.ProjectNo,
                QuoteNo = offer.QuoteNo,
                TermsOfDelivery = offer.TermsOfDelivery,
                TermsOfPayment = offer.TermsOfPayment,
                To = inquiry.To,
                VAT = offer.VAT,
                TotalAmount = itemsView.Sum(s => s.Amount)
            };

            model.VATAmount = Math.Round(model.TotalAmount * model.VAT / 100M, 2);
            model.QuotationTotal = model.TotalAmount + model.VATAmount;
            return model;
        }

        public JsonResult GetInquriesAsSelectList()
        {
            Inquiry[] inquries = db.Inquiry.AsNoTracking().OrderByDescending(o => o.InquiryNo).ToArray();
            SelectList selectList = new SelectList(inquries, "InquiryId", "InquiryNo");
            return Json(selectList, JsonRequestBehavior.AllowGet);
        }

        public JsonResult CreateOfferPdf(int? BeslOfferId)
        {
            if (BeslOfferId == null)
            {
                return Json(new { JsonResultMainReturn = new JsonResultMainReturn(false, "OfferIdIsNull") }, JsonRequestBehavior.AllowGet);
            }
            JsonResultMainReturn dataRet = new PdfDocs().CreateOffer(BeslOfferId.Value);
            return Json(new { JsonResultMainReturn = dataRet }, JsonRequestBehavior.AllowGet);
        }

        public FileResult DownloadOfferPdf(int? BeslOfferId)
        {
            if (BeslOfferId == null)
            {
                return null;
            }
            try
            {
                string fileName = new PdfDocs().getOfferFileName(BeslOfferId.Value);
                string folderName = "Offers";
                string subPath = Server.MapPath("~/Content/Docs") + "\\" + folderName;

                return new PrintController().DownloadFile(subPath, fileName, fileName);
            }
            catch
            {
                return null;
            }
        }

        #region edit tool methods

        public ActionResult EditOffer(int BeslOfferId)
        {
            if (BeslOfferId == 0)
            {
                return RedirectToAction("Index");
            }

            BeslOffer beslOffer = db.BeslOffer.AsNoTracking().FirstOrDefault(f => f.BeslOfferId == BeslOfferId);
            if (beslOffer == null)
            {
                return RedirectToAction("Index");
            }

            Inquiry inquiry = db.Inquiry.Find(beslOffer.InquiryId);
            BeslOfferItem[] beslOfferItems = db.BeslOfferItem.AsNoTracking().Where(s => s.BeslOfferId == beslOffer.BeslOfferId).ToArray();

            EditOfferView offerItem = new EditOfferView();
            offerItem.Offer = beslOffer;
            offerItem.OfferItems = beslOfferItems;
            offerItem.EnquiryQuoteNo = beslOffer.QuoteNo + " - " + inquiry.InquiryNo;
            offerItem.FactorMarginTbl = calculateFactorMarginTbl(null, beslOfferItems);

            return View(offerItem);
        }

        public JsonResult EditBeslOfferItemsFromTool(OfferItemRowInEditTool[] list, int beslOfferId)
        {
            try
            {
                //BeslOfferItem[] beslOfferItems = db.BeslOfferItem.Where(s => s.BeslOfferId == beslOfferId).ToArray();
                //foreach (BeslOfferItem item in beslOfferItems)
                //{
                //    db.BeslOfferItem.Remove(item);
                //}
                //db.SaveChanges();
                //db.Database.ExecuteSqlCommand("declare @max int select @max=max([BeslOfferItemId])from [BeslOfferItems] if @max IS NUll SET @max = 0 DBCC CHECKIDENT ('[BeslOfferItems]', RESEED,@max)");
                //if (list != null)
                //{
                //    BeslOfferItem[] offerItems = cleanBeslOfferItemsFromTool(list, beslOfferId);
                //    foreach (var item in offerItems)
                //    {
                //        db.BeslOfferItem.Add(item);
                //    }
                //    db.SaveChanges();
                //}

                BeslOfferItem[] offerItemsFromDb = db.BeslOfferItem.Where(s => s.BeslOfferId == beslOfferId).ToArray();
                if (list == null || list.Length == 0)
                {
                    foreach (BeslOfferItem item in offerItemsFromDb)
                    {
                        db.BeslOfferItem.Remove(item);
                    }
                    db.SaveChanges();
                }
                else if (offerItemsFromDb == null || offerItemsFromDb.Length == 0)
                {
                    BeslOfferItem[] newOfferItems = cleanBeslOfferItemsFromTool(list, beslOfferId);
                    foreach (var item in newOfferItems)
                    {
                        db.BeslOfferItem.Add(item);
                    }
                    db.SaveChanges();
                }
                else
                {
                    BeslOfferItem[] newOfferItems = cleanBeslOfferItemsFromTool(list, beslOfferId);
                    if (newOfferItems.Length > offerItemsFromDb.Length)
                    {
                        for (int i = 0; i < offerItemsFromDb.Length; i++)
                        {
                            editDbOfferItem(offerItemsFromDb[i], newOfferItems[i]);
                        }
                        for (int i = offerItemsFromDb.Length; i < newOfferItems.Length; i++)
                        {
                            db.BeslOfferItem.Add(newOfferItems[i]);
                        }
                    }
                    else if (newOfferItems.Length == offerItemsFromDb.Length)
                    {
                        for (int i = 0; i < offerItemsFromDb.Length; i++)
                        {
                            editDbOfferItem(offerItemsFromDb[i], newOfferItems[i]);
                        }
                    }
                    else if (newOfferItems.Length < offerItemsFromDb.Length)
                    {
                        for (int i = 0; i < newOfferItems.Length; i++)
                        {
                            editDbOfferItem(offerItemsFromDb[i], newOfferItems[i]);
                        }
                        for (int i = newOfferItems.Length; i < offerItemsFromDb.Length; i++)
                        {
                            db.BeslOfferItem.Remove(offerItemsFromDb[i]);
                        }
                    }
                    db.SaveChanges();
                }

                return Json(new { JsonResultMainReturn = new JsonResultMainReturn(true, "Success") }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                return Json(new { JsonResultMainReturn = new JsonResultMainReturn(false, ex.Message) }, JsonRequestBehavior.AllowGet);
            }
        }

        private void editDbOfferItem(BeslOfferItem oldItem, BeslOfferItem newItem)
        {
            oldItem.Amount = newItem.Amount;
            oldItem.Comments = newItem.Comments;
            oldItem.DeliveryDuration = newItem.DeliveryDuration;
            oldItem.Description = newItem.Description;
            oldItem.DescriptionForSale = newItem.DescriptionForSale;
            oldItem.Quantity = newItem.Quantity;
            oldItem.Unit = newItem.Unit;
            oldItem.UnitPrice = newItem.UnitPrice;
        }

        public JsonResult TestBeslOfferItemsFromTool(OfferItemRowInEditTool[] list, int beslOfferId)
        {
            try
            {
                BeslOfferItem[] items = cleanBeslOfferItemsFromTool(list, beslOfferId);
                return Json(
                    new
                    {
                        JsonResultMainReturn = new JsonResultMainReturn(true, "Success"),
                        BeslOfferItems = items,
                        TotalAmount = items.Sum(s => s.Amount)
                    },
                    JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                return Json(new { JsonResultMainReturn = new JsonResultMainReturn(false, ex.Message) }, JsonRequestBehavior.AllowGet);
            }
        }

        public JsonResult ApplyFactorMarginOfferFromTool(OfferItemRowInEditTool[] list, FactorMarginTbl FmTbl, int beslOfferId)
        {
            try
            {
                BeslOfferItem[] items = cleanBeslOfferItemsFromTool(list, beslOfferId);
                applyFactorMargin(FmTbl, items);
                FmTbl = calculateFactorMarginTbl(FmTbl, items);
                return Json(new { JsonResultMainReturn = new JsonResultMainReturn(true, "Success"), BeslOfferItems = items, FactorMarginTbl = FmTbl }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                return Json(new { JsonResultMainReturn = new JsonResultMainReturn(false, ex.Message) }, JsonRequestBehavior.AllowGet);
            }
        }

        private BeslOfferItem[] cleanBeslOfferItemsFromTool(OfferItemRowInEditTool[] offerItemListFromTool, int beslOfferId)
        {
            BeslOfferItem[] offerItems = new BeslOfferItem[offerItemListFromTool.Length];
            for (int i = 0; i < offerItemListFromTool.Length; i++)
            {
                var item = offerItemListFromTool[i];
                BeslOfferItem sss = new BeslOfferItem
                {
                    BeslOfferId = beslOfferId,
                    Description = item.description == null ? " " : item.description,
                    DescriptionForSale = item.descriptionforsale == null ? " " : item.descriptionforsale,
                    //Quantity = Decimal.Parse(item.quantity),
                    Unit = item.unit == null ? " " : item.unit,
                    //UnitPrice = Decimal.Parse(item.unitprice),
                    //Amount = Decimal.Parse(item.quantity) * Decimal.Parse(item.unitprice),
                    DeliveryDuration = item.deliveryduration == null ? " " : item.deliveryduration,
                    Comments = item.comments == null ? " " : item.comments
                };

                decimal val;
                //bos gonderende osibka verir
                if (item.quantity == null || !decimal.TryParse(item.quantity.Trim(new char[] { '.', ',', ' ' }).Replace(",", "."), NumberStyles.Number, CultureInfo.InvariantCulture, out val))
                {
                    val = 0M;
                }
                sss.Quantity = val;

                if (item.unitprice == null || !decimal.TryParse(item.unitprice.Trim(new char[] { '.', ',', ' ' }).Replace(",", "."), NumberStyles.Number, CultureInfo.InvariantCulture, out val))
                {
                    val = 0M;
                }
                sss.UnitPrice = val;

                //?????????? ?????????????? ????????????? ???????? ???????? ??????
                double unitPrice = Convert.ToDouble(sss.UnitPrice);
                double quantity = Convert.ToDouble(sss.Quantity);
                decimal amount = (decimal)unitPrice * (decimal)quantity;
                double d = (double)amount;
                amount = (decimal)Math.Round(d, 2);
                //amount = (double)(1.013M * 5M); 1.013-u 5-e vuranda 5.064999999999995 eliyir .... !!!!
                sss.Amount = amount;
                //??????? ??????????? ?????? ????????? ??????????? ???????? ??????

                offerItems[i] = sss;
            }
            return offerItems;
        }

        private FactorMarginTbl calculateFactorMarginTbl(FactorMarginTbl fmTbl, BeslOfferItem[] offerItems)
        {
            if (fmTbl == null)
            {
                fmTbl = new FactorMarginTbl();
                fmTbl.Amount = offerItems.Sum(s => s.Amount) + "";
            }
            else
            {
                fmTbl.PreAmount = fmTbl.Amount;
                fmTbl.PreFactor = fmTbl.Factor;
                fmTbl.PreMargin = fmTbl.Margin;
                fmTbl.Amount = offerItems.Sum(s => s.Amount) + "";
                fmTbl.Factor = "";
                fmTbl.Margin = "";
            }
            return fmTbl;
        }

        private void applyFactorMargin(FactorMarginTbl fmTbl, BeslOfferItem[] offerItems)
        {
            decimal margin;
            decimal factor;
            //bos gonderende osibka verir
            if (fmTbl.Margin == null || !decimal.TryParse(fmTbl.Margin.Trim(new char[] { '.', ',', ' ' }).Replace(",", "."), NumberStyles.Number, CultureInfo.InvariantCulture, out margin))
            {
                margin = 0M;
            }
            if (fmTbl.Factor == null || !decimal.TryParse(fmTbl.Factor.Trim(new char[] { '.', ',', ' ' }).Replace(",", "."), NumberStyles.Number, CultureInfo.InvariantCulture, out factor))
            {
                factor = 1M;
            }

            foreach (var item in offerItems)
            {
                item.UnitPrice = Math.Round(item.UnitPrice * factor, 2);
                item.Amount = Math.Round(item.UnitPrice * item.Quantity, 2);
            }

            decimal totalAmount = offerItems.Sum(s => s.Amount);
            if (margin > 0)
            {
                foreach (var item in offerItems)
                {
                    item.UnitPrice = Math.Round(item.Amount / totalAmount * (totalAmount + margin) / item.Quantity, 2);
                    item.Amount = Math.Round(item.UnitPrice * item.Quantity, 2);
                }
            }
        }

        #endregion

        public string getNewQuoteNo()
        {
            CompanyDetails compDet = db.CompanyDetails.FirstOrDefault();
            string prefix;
            string pattern = "";
            if (compDet != null)
            {
                prefix = compDet.OfferPrefix;
                char[] tempPrefixChars = compDet.OfferPrefix.ToCharArray();
                for (int i = 0; i < tempPrefixChars.Length; i++)
                {
                    pattern += "[a-zA-Z]";
                }
                pattern += "\\s";
                for (int i = 0; i < compDet.OfferNoLength; i++)
                {
                    pattern += "[0-9]";
                }
            }
            else
            {
                prefix = "XXXX";
                for (int i = 0; i < 4; i++)
                {
                    pattern += "[a-zA-Z]";
                }
                pattern += "\\s";
                for (int i = 0; i < 6; i++)
                {
                    pattern += "[0-9]";
                }
            }

            var beslOffers = db.BeslOffer.OrderByDescending(o => o.DateOffer).ToArray();
            BeslOffer lastOffer = beslOffers.Where(w => Regex.IsMatch(w.QuoteNo, pattern)).OrderByDescending(o => o.QuoteNo).FirstOrDefault();
            string lastOfferQuoteNo;
            if (lastOffer != null) { lastOfferQuoteNo = lastOffer.QuoteNo.Trim(); } else { lastOfferQuoteNo = prefix + " 000000"; }

            int counter = 0;
            int lastQuoteNumber = 0;
            char[] quoteChars = lastOfferQuoteNo.ToCharArray();
            int temp;

            for (int i = quoteChars.Length - 1; i >= 0; i--)
            {
                if (int.TryParse(quoteChars[i].ToString(), out temp))
                {
                    lastQuoteNumber += (int)Math.Pow(10, counter) * temp;
                    counter++;
                }
                else
                {
                    break;
                }
            }
            return prefix + " " + (++lastQuoteNumber).ToString().PadLeft(6, '0'); ;
        }

        public JsonResult GetNewQuoteNoForPopUp()
        {
            try
            {
                string quoteNo = getNewQuoteNo();
                return Json(new { QuoteNo = quoteNo, JsonResultMainReturn = new JsonResultMainReturn(true, "Success") }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                return Json(new { JsonResultMainReturn = new JsonResultMainReturn(false, ex.Message) }, JsonRequestBehavior.AllowGet);
            }
        }

        #region OfferCRUD
        public ActionResult BeslOfferRead([DataSourceRequest]DataSourceRequest request)
        {
            var inquiryList = db.Inquiry.AsNoTracking();
            BeslOffer[] beslOffers = db.BeslOffer.AsNoTracking().ToArray();
            foreach (var item in beslOffers)
            {
                var inqu = inquiryList.FirstOrDefault(f => f.InquiryId == item.InquiryId);
                if (inqu != null)
                {
                    item.InquiryNo = inqu.InquiryNo;
                }
            }
            return Json(beslOffers.ToDataSourceResult(request));
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult BeslOfferCreate([DataSourceRequest]DataSourceRequest request, BeslOffer beslOffer)
        {
            if (ModelState.IsValid)
            {
                db.BeslOffer.Add(beslOffer);
                db.SaveChanges();
            }
            return Json(new[] { beslOffer }.ToDataSourceResult(request, ModelState));
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult BeslOfferUpdate([DataSourceRequest]DataSourceRequest request, BeslOffer beslOffer)
        {
            if (ModelState.IsValid)
            {
                BeslOffer offerInDb = db.BeslOffer.FirstOrDefault(f => f.BeslOfferId == beslOffer.BeslOfferId);
                offerInDb.Currency = beslOffer.Currency;
                offerInDb.DateOffer = beslOffer.DateOffer;
                offerInDb.InquiryId = beslOffer.InquiryId;
                offerInDb.QuoteNo = beslOffer.QuoteNo;
                offerInDb.VAT = beslOffer.VAT;
                offerInDb.TermsOfDelivery = beslOffer.TermsOfDelivery;
                offerInDb.TermsOfPayment = beslOffer.TermsOfPayment;

                db.SaveChanges();
            }
            return Json(new[] { beslOffer }.ToDataSourceResult(request, ModelState));
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult BeslOfferDelete([DataSourceRequest]DataSourceRequest request, BeslOffer beslOffer)
        {
            if (ModelState.IsValid)
            {
                BeslOffer offerInDb = db.BeslOffer.FirstOrDefault(f => f.BeslOfferId == beslOffer.BeslOfferId);
                if (offerInDb != null)
                {
                    List<BeslOfferItem> itemList = db.BeslOfferItem.Where(w => w.BeslOfferId == beslOffer.BeslOfferId).ToList();
                    if (itemList.Count > 0)
                    {
                        for (int i = 0; i < itemList.Count; i++)
                        {
                            db.BeslOfferItem.Remove(itemList[i]);
                        }
                    }
                    db.BeslOffer.Remove(offerInDb);
                    db.SaveChanges();
                }
            }
            return Json(new[] { beslOffer }.ToDataSourceResult(request, ModelState));
        }
        #endregion

        #region OfferItemCRUD
        public ActionResult BeslOfferItemRead([DataSourceRequest]DataSourceRequest request, int id)
        {
            BeslOfferItem[] offerItems = db.BeslOfferItem.AsNoTracking().Where(w => w.BeslOfferId == id).ToArray();
            int itemCount = offerItems.Count();
            BeslOfferItemView[] offerItemsView = new BeslOfferItemView[itemCount];
            for (int i = 0; i < itemCount; i++)
            {
                offerItemsView[i] = new BeslOfferItemView(offerItems[i]);
            }
            return Json(offerItemsView.ToDataSourceResult(request));
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult BeslOfferItemCreate([DataSourceRequest]DataSourceRequest request, BeslOfferItemView beslOfferItemView, int id)
        {
            if (ModelState.IsValid)
            {
                beslOfferItemView.BeslOfferId = id;
                db.BeslOfferItem.Add(new BeslOfferItem(beslOfferItemView));
                db.SaveChanges();
            }
            return Json(new[] { beslOfferItemView }.ToDataSourceResult(request, ModelState));
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult BeslOfferItemUpdate([DataSourceRequest]DataSourceRequest request, BeslOfferItemView beslOfferItemView)
        {
            if (ModelState.IsValid)
            {
                BeslOfferItem itemInDb = db.BeslOfferItem.FirstOrDefault(f => f.BeslOfferItemId == beslOfferItemView.BeslOfferItemId);
                itemInDb.Amount = beslOfferItemView.Amount;
                itemInDb.BeslOfferId = beslOfferItemView.BeslOfferId;
                itemInDb.Comments = beslOfferItemView.Comments;
                itemInDb.DeliveryDuration = beslOfferItemView.DeliveryDuration;
                itemInDb.Description = beslOfferItemView.Description;
                itemInDb.DescriptionForSale = beslOfferItemView.DescriptionForSale;
                itemInDb.Quantity = beslOfferItemView.Quantity;
                itemInDb.Unit = beslOfferItemView.Unit;
                itemInDb.UnitPrice = beslOfferItemView.UnitPrice;
                db.SaveChanges();
            }
            return Json(new[] { beslOfferItemView }.ToDataSourceResult(request, ModelState));
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult BeslOfferItemDelete([DataSourceRequest]DataSourceRequest request, BeslOfferItem beslOfferItemView)
        {
            if (ModelState.IsValid)
            {
                BeslOfferItem offerItemInDb = db.BeslOfferItem.FirstOrDefault(f => f.BeslOfferItemId == beslOfferItemView.BeslOfferItemId);
                if (offerItemInDb != null)
                {
                    db.BeslOfferItem.Remove(offerItemInDb);
                    db.SaveChanges();
                }
            }
            return Json(new[] { beslOfferItemView }.ToDataSourceResult(request, ModelState));
        }
        #endregion

    }
}