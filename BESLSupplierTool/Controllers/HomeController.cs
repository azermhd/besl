﻿using System;
using System.Web;
using System.Web.Mvc;
using System.Globalization;
using System.Threading;
using BESLSupplierTool.BusinessLayer.Docs;
using BESLSupplierTool.Models.ReturnModels;
using System.IO;

namespace BESLSupplierTool.Controllers
{
    public class HomeController : Controller
    {
        public ActionResult Index()
        {
            //string lang = "az";
            //var cultureInfo = new CultureInfo(lang);
            //Thread.CurrentThread.CurrentUICulture = cultureInfo;
            //Thread.CurrentThread.CurrentCulture = CultureInfo.CreateSpecificCulture(cultureInfo.Name);
            //HttpCookie langCookie = new HttpCookie("culture", lang);
            //langCookie.Expires = DateTime.Now.AddYears(1);
            //System.Web.HttpContext.Current.Response.Cookies.Add(langCookie);

            return View();
        }
      
        public ActionResult About()
        {
            ViewBag.Message = "Your application description page.";

            return View();
        }

        public ActionResult Contact()
        {
            ViewBag.Message = "Your contact page.";

            return View();
        }
    }
}