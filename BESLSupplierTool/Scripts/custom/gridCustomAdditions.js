﻿function onEdit(e) {
    if (e.model.isNew()) {
        var update = $(e.container).parent().find(".k-grid-update");
        //var cancel = $(e.container).parent().find(".k-grid-cancel");
        $(update).html('<span class="k-icon k-update"></span>' + "Add");
        //$(cancel).html('<span class="k-icon k-cancel"></span>' + "Close");
    }
    addLoadingGifToDialog(e);
}

function addLoadingGifToDialog(e) {
    var updateButton = $(e.container).parent().find(".k-grid-update");
    updateButton.unbind('click').click(function (event) {
        var dialogBox = $(".k-popup-edit-form.k-window-content.k-content");
        dialogBox.append('<div class="dialog-loading-overlay" style="!important;position: absolute;width: 100%;height: 100%; left: 0; top: 0; background-color: #dedede; opacity: 0.5; background-image: url(/Content/img/loadingForm.gif);background-repeat:no-repeat;background-position: center;background-size: 60px 60px; z-index:10000;"></div>');
        setTimeout(function () {
            var invalidInputList = $(".k-invalid").toArray();
            if (invalidInputList.length > 0) {
                $(".dialog-loading-overlay").remove();
            }
        }, 400);
    });
}

function grid_error(e) {
    if (e.errors) {
        var message = window.errorMessage + ":\n";

        $.each(e.errors, function (key, value) {
            if ('errors' in value) {
                $.each(value.errors, function () {
                    message += this + "\n";
                });
            }
        });
        $(".dialog-loading-overlay").remove();
        alert(message);
        //this is magic
        // prevent popup from closing
        throw new Error('This is not an error. This is just to abort javascript');
    }
}

function refreshGrid() {
    this.read();
}

function refreshKendoGrid(gridId) {
    $('#' + gridId).data('kendoGrid').dataSource.read();
    $('#' + gridId).data('kendoGrid').refresh();
}

function removeHrefFromGridCustomRefreshButton() {
    $(".gridCustomRefreshButton").removeAttr("href");
}

//make value of required kendoDatepickers null which they have "required-date-default-null" class
//$(document).ready(function () {
//    $(document).on("click", ".k-grid-add", function () {
//        var x = document.getElementsByClassName("required-date-default-null");
//        var i;
//        for (i = 0; i < x.length; i++) {
//            if (x[i].nodeName == "INPUT") {
//                var dp = $("#" + x[i].id).data("kendoDatePicker");
//                dp.value("");
//                dp.trigger("change");
//            }
//        }
//    });
//}); 