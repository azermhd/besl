﻿function openNav() {
    $("#sideMenuButtonDiv").fadeOut(300);
    $(".sidenav a").hide();
    $(".sidenav a").fadeIn(500);
    document.getElementById("mySidenav").style.width = "250px";
    document.getElementById("navbar").style.marginLeft = "250px";
    document.getElementById("body-content").style.paddingLeft = "250px";
}

function closeNav() {
    $("#sideMenuButtonDiv").fadeIn(500);
    $(".sidenav a").fadeOut(30);
    document.getElementById("mySidenav").style.width = "0";
    document.getElementById("navbar").style.marginLeft = "0";
    document.getElementById("body-content").style.paddingLeft = "15px";
}
