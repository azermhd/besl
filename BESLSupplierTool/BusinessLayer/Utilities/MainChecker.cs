﻿using BESLSupplierTool.Models.DBModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace BESLSupplierTool.BusinessLayer.Utilities
{

    public class MainChecker
    {
        DatabaseContext db;
        public MainChecker()
        {
            db = new DatabaseContext();
        }

        public string check(Inquiry inquiry)
        {
            string result = "";
            Inquiry inqu = db.Inquiry.AsNoTracking().FirstOrDefault(f => f.InquiryNo.ToLower() == inquiry.InquiryNo.ToLower());
            if (inqu != null && inqu.InquiryId != inquiry.InquiryId)
            {
                result += "Eyni sorğu nömrəsi artıq mövcuddur\n";
            }
            return result;
        }

        public string check(BeslOffer beslOffer)
        {
            string result = "";
            BeslOffer offer = db.BeslOffer.AsNoTracking().FirstOrDefault(f => f.InquiryNo.ToLower() == beslOffer.InquiryNo.ToLower());
            if (offer != null && offer.BeslOfferId != offer.BeslOfferId)
            {
                result += "Eyni sorğu nömrəsi artıq mövcuddur\n";
            }
            return result;
        }
    }
}