﻿using BESLSupplierTool.Models.DBModels;
using BESLSupplierTool.Models.ReturnModels;
using GemBox.Spreadsheet;
using System.Collections.Generic;
using System.Drawing;
using System.Globalization;
using System.Text;
using System.Web;

namespace BESLSupplierTool.BusinessLayer.Docs
{
    public class ExcelWorksheets
    {
        public void testExcel()
        {
            SpreadsheetInfo.SetLicense("FREE-LIMITED-KEY");
            //FontSettings.FontsBaseDirectory = HttpContext.Current.Server.MapPath("C:/Windows/Fonts/");
            FontSettings.FontsBaseDirectory = "C:/Windows/Fonts/";
            string folderpath = HttpContext.Current.Server.MapPath("~/AppData/Offers");
            ExcelFile ef = new ExcelFile();
            CellRange range;
            CellStyle style;
            style = new CellStyle();
            CellStyle tmpStyle = new CellStyle();
            tmpStyle.HorizontalAlignment = HorizontalAlignmentStyle.Center;
            tmpStyle.VerticalAlignment = VerticalAlignmentStyle.Center;
            tmpStyle.FillPattern.SetSolid(ColorTranslator.FromHtml("#a5ffe4"));
            tmpStyle.Font.Weight = ExcelFont.BoldWeight;
            tmpStyle.Font.Color = Color.White;
            tmpStyle.WrapText = true;
            tmpStyle.Borders.SetBorders(MultipleBorders.Right | MultipleBorders.Top, Color.Black, LineStyle.Thick);
            ExcelWorksheet ws = ef.Worksheets.Add("Work");
            //ef.DefaultFontName = "Calibri";
            //CellRange range = ws.Cells.GetSubrange("a1","b5");
            //range.Value = "dsaas";
            ws.Cells[15, 8].Value = "31";
            range = ws.Cells.GetSubrange("B1", "C2");
            range.Merged = true;
            range.Value = 31;
            range.Style = tmpStyle;

            ef.Save("D:\\BitBucket\\BESLSupplierTool\\Content\\Docs\\Offers\\test.xlsx");

        }

        public void uploadExcel(JsonResultMainReturn xx)
        {
            // If using Professional version, put your serial key below.
            SpreadsheetInfo.SetLicense("FREE-LIMITED-KEY");

            ExcelFile ef = ExcelFile.Load("D:\\BitBucket\\BESLSupplierTool\\Content\\Docs\\Offers\\test2.xlsx");

            StringBuilder sb = new StringBuilder();

            // Iterate through all worksheets in an Excel workbook.
            foreach (ExcelWorksheet sheet in ef.Worksheets)
            {
                sb.AppendLine();
                sb.AppendFormat("{0} {1} {0}", new string('-', 25), sheet.Name);

                // Iterate through all rows in an Excel worksheet.
                foreach (ExcelRow row in sheet.Rows)
                {
                    sb.AppendLine();
                    sb.Append("<br />");
                    // Iterate through all allocated cells in an Excel row.
                    foreach (ExcelCell cell in row.AllocatedCells)
                    {
                        if (cell.ValueType != CellValueType.Null)
                            sb.Append(string.Format("{0} [{1}]", cell.Value, cell.ValueType).PadRight(25));
                        else
                            sb.Append(new string('*', 25));
                    }
                    sb.Append("<br />");
                }
            }
            xx.Message = sb.ToString();
        }

        public InquiryItemView[] readInquiryItemsFromExcel(string fileName)
        {
            // If using Professional version, put your serial key below.
            SpreadsheetInfo.SetLicense("FREE-LIMITED-KEY");

            ExcelFile ef = ExcelFile.Load(HttpContext.Current.Server.MapPath("~/Content/Docs/Inquiries/") + "\\" + fileName);
            List<InquiryItemView> inquiryItems = new List<InquiryItemView>();

            InquiryItemView inqu;
            string quantity;
            foreach (ExcelWorksheet sheet in ef.Worksheets)
            {
                foreach (ExcelRow row in sheet.Rows)
                {
                    inqu = new InquiryItemView();
                    if (row.Cells[0, 0].Value == null)
                        row.Cells[0, 0].Value = " ";
                    if (row.Cells[0, 1].Value == null)
                        row.Cells[0, 1].Value = " ";
                    if (row.Cells[0, 2].Value == null)
                        row.Cells[0, 2].Value = 0;

                    inqu.Description = row.Cells[0, 0].Value.ToString();                   
                    inqu.Unit = row.Cells[0, 1].Value.ToString();
                    quantity = row.Cells[0, 2].Value.ToString();
                    decimal val;
                    if (quantity == null || !decimal.TryParse(quantity.Trim(new char[] { '.', ',', ' ' }).Replace(",", "."), NumberStyles.Number, CultureInfo.InvariantCulture, out val))
                    {
                        val = 0M;
                    }
                    inqu.Quantity = val;
                    inquiryItems.Add(inqu);
                }
            }
            return inquiryItems.ToArray();
        }
    }
}