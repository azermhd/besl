﻿using BESLSupplierTool.Controllers;
using BESLSupplierTool.Models.DBModels;
using BESLSupplierTool.Models.ReturnModels;
using BESLSupplierTool.Models.ViewModels;
using iTextSharp.text;
using iTextSharp.text.pdf;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.IO;
using System.Linq;
using System.Web;

namespace BESLSupplierTool.BusinessLayer.Docs
{
    public class PdfDocs
    {
        DatabaseContext db;

        public PdfDocs()
        {
            db = new DatabaseContext();
        }
        public JsonResultMainReturn CreateOffer(int offerId)
        {
            Document doc;
            try
            {
                PrintOffer offerData;
                offerData = getOfferData(offerId);
                if (offerData == null)
                {
                    return new JsonResultMainReturn(false, "OfferNotFound");
                }

                doc = new Document(iTextSharp.text.PageSize.A4.Rotate(), 30, 30, 30, 30);

                PdfPTable table;
                PdfPTable tableInner;
                PdfPTable tableOuter;
                PdfPCell cell;
                Paragraph para;
                Phrase phrase;
                Font boldFont = new Font(Font.FontFamily.UNDEFINED, 10, Font.BOLD);
                Font normalFont = new Font(Font.FontFamily.UNDEFINED, 10, Font.NORMAL);

                string folderName = "Offers";
                string subPath = HttpContext.Current.Server.MapPath("~/Content/Docs") + "\\" + folderName;
                PdfWriter writer = PdfWriter.GetInstance(doc, new FileStream(subPath + "\\" + getOfferFileName(offerId), FileMode.Create));

                string ARIAL = Path.Combine(Environment.GetFolderPath(Environment.SpecialFolder.Fonts), "ARIAL.TTF");
                BaseFont bf = BaseFont.CreateFont(ARIAL, BaseFont.IDENTITY_H, BaseFont.NOT_EMBEDDED);
                Font arial1 = new Font(bf, 1, Font.NORMAL);
                Font arial8 = new Font(bf, 8, Font.NORMAL);
                Font arial9 = new Font(bf, 9, Font.NORMAL);
                Font arial10 = new Font(bf, 10, Font.NORMAL);
                Font arial11 = new Font(bf, 11, Font.NORMAL);
                Font arial12 = new Font(bf, 12, Font.NORMAL);
                Font arial13 = new Font(bf, 13, Font.NORMAL);
                Font arial14 = new Font(bf, 14, Font.NORMAL);
                Font arial15 = new Font(bf, 15, Font.NORMAL);
                Font arialBold9 = new Font(bf, 9, Font.BOLD);

                string imageFolder = HttpContext.Current.Server.MapPath("~/Content/images");
                Image logo = Image.GetInstance(imageFolder + "\\logo.png");
                logo.ScalePercent(128f / logo.Width * 100);

                doc.Open();

                Chunk slashN = new Chunk("\n");
                Paragraph slashNPara = new Paragraph("\n");
                //doc.Add(slashNPara);
                //doc.Add(slashNPara);

                PdfPTable testta = new PdfPTable(new float[] { 110, 100 });
                testta.LockedWidth = true;
                testta.TotalWidth = 260;

                cell = new PdfPCell(logo, false);
                cell.BorderWidth = 0;
                cell.Padding = 0;
                cell.PaddingRight = 10;
                cell.VerticalAlignment = Element.ALIGN_CENTER;
                testta.AddCell(cell);

                cell = new PdfPCell();
                para = new Paragraph(17, "BAKU\nENGINEERING\nSUPPLIES LTD", arial12);
                cell.AddElement(para);
                cell.VerticalAlignment = Element.ALIGN_CENTER;
                cell.BorderWidth = 0;
                cell.BorderWidthLeft = 1;
                cell.Padding = 0;
                cell.PaddingLeft = 13;
                cell.PaddingBottom = 6;
                testta.AddCell(cell);

                #region header
                table = new PdfPTable(new float[] { 200, 120 });
                table.TotalWidth = 660;
                table.LockedWidth = true;
                table.HorizontalAlignment = Element.ALIGN_CENTER;

                cell = new PdfPCell();
                testta.HorizontalAlignment = Element.ALIGN_LEFT;
                cell.AddElement(testta);
                cell.HorizontalAlignment = Element.ALIGN_LEFT;
                cell.VerticalAlignment = Element.ALIGN_CENTER;
                cell.BorderWidth = 0;
                cell.Padding = 15;
                table.AddCell(cell);

                string address1 = "Adress: M.Hadi 7, Baku\\Azerbaijan, AZ1142\n";
                string address2 = "Phone: (+994 12) 370 01 92 \n";
                string address3 = "Fax: (+994 12) 370 94 36 \n";
                string address4 = "Mail: info @bakuengineering.com \n";
                string address5 = "Web: www.bakuengineering.com";

                cell = new PdfPCell();
                para = new Paragraph(13, address1 + address2 + address3 + address4 + address5, arial10);
                cell.AddElement(para);
                cell.BorderWidth = 0;
                cell.VerticalAlignment = Element.ALIGN_CENTER;

                table.AddCell(cell);
                doc.Add(table);

                table = new PdfPTable(new float[] { 100, 100 });
                table.TotalWidth = 600;
                table.LockedWidth = true;
                table.HorizontalAlignment = Element.ALIGN_CENTER;

                tableInner = new PdfPTable(new float[] { 60, 100 });
                tableInner.TotalWidth = 180;
                tableInner.LockedWidth = true;

                cell = new PdfPCell(new Paragraph("To: ", arial10));
                cell.HorizontalAlignment = Element.ALIGN_RIGHT;
                setHeaderTableCell(cell);
                tableInner.AddCell(cell);

                cell = new PdfPCell(new Paragraph(offerData.To, arial10));
                cell.HorizontalAlignment = Element.ALIGN_LEFT;
                setHeaderTableCell(cell);
                tableInner.AddCell(cell);

                cell = new PdfPCell(new Paragraph("Buyer: ", arial10));
                cell.HorizontalAlignment = Element.ALIGN_RIGHT;
                setHeaderTableCell(cell);
                tableInner.AddCell(cell);

                cell = new PdfPCell(new Paragraph(offerData.Buyer, arial10));
                cell.HorizontalAlignment = Element.ALIGN_LEFT;
                setHeaderTableCell(cell);
                tableInner.AddCell(cell);

                cell = new PdfPCell(new Paragraph("Attn: ", arial10));
                cell.HorizontalAlignment = Element.ALIGN_RIGHT;
                setHeaderTableCell(cell);
                tableInner.AddCell(cell);

                cell = new PdfPCell(new Paragraph(offerData.Attn, arial10));
                cell.HorizontalAlignment = Element.ALIGN_LEFT;
                setHeaderTableCell(cell);
                tableInner.AddCell(cell);

                cell = new PdfPCell(new Paragraph("Date: ", arial10));
                cell.HorizontalAlignment = Element.ALIGN_RIGHT;
                setHeaderTableCell(cell);
                tableInner.AddCell(cell);

                cell = new PdfPCell(new Paragraph(offerData.Date, arial10));
                cell.HorizontalAlignment = Element.ALIGN_LEFT;
                setHeaderTableCell(cell);
                tableInner.AddCell(cell);

                cell = new PdfPCell(tableInner);
                cell.BorderWidth = 0;
                table.AddCell(cell);

                tableInner = new PdfPTable(new float[] { 80, 100 });
                tableInner.TotalWidth = 180;
                tableInner.LockedWidth = true;

                cell = new PdfPCell(new Paragraph("Inquiry №:", arial10));
                cell.HorizontalAlignment = Element.ALIGN_RIGHT;
                setHeaderTableCell(cell);
                tableInner.AddCell(cell);

                cell = new PdfPCell(new Paragraph(offerData.InquiryNo, arial10));
                cell.HorizontalAlignment = Element.ALIGN_LEFT;
                setHeaderTableCell(cell);
                tableInner.AddCell(cell);

                cell = new PdfPCell(new Paragraph("Project №:", arial10));
                cell.HorizontalAlignment = Element.ALIGN_RIGHT;
                setHeaderTableCell(cell);
                tableInner.AddCell(cell);

                cell = new PdfPCell(new Paragraph(offerData.ProjectNo, arial10));
                cell.HorizontalAlignment = Element.ALIGN_LEFT;
                cell.BorderWidth = 0;
                tableInner.AddCell(cell);

                cell = new PdfPCell(new Paragraph("Quote №:", arial10));
                cell.HorizontalAlignment = Element.ALIGN_RIGHT;
                setHeaderTableCell(cell);
                tableInner.AddCell(cell);

                cell = new PdfPCell(new Paragraph(offerData.QuoteNo, arial10));
                cell.HorizontalAlignment = Element.ALIGN_LEFT;
                setHeaderTableCell(cell);
                tableInner.AddCell(cell);

                cell = new PdfPCell(tableInner);
                cell.BorderWidth = 0;
                table.AddCell(cell);
                doc.Add(table);
                doc.Add(slashNPara);

                #endregion

                #region body

                table = new PdfPTable(new float[] { 60, 780, 120, 140, 190, 180, 270, 270 });
                table.TotalWidth = 755;
                table.LockedWidth = true;
                table.HorizontalAlignment = Element.ALIGN_CENTER;

                cell = new PdfPCell(new Paragraph("№", arial9));
                setCellBeslMainHeader(cell);
                cell.PaddingTop = 7;
                table.AddCell(cell);

                cell = new PdfPCell(new Paragraph("Açıqlama /\nDescription", arial9));
                setCellBeslMainHeader(cell);
                table.AddCell(cell);

                cell = new PdfPCell(new Paragraph("Vahid /\nUnit", arial9));
                setCellBeslMainHeader(cell);
                table.AddCell(cell);

                cell = new PdfPCell(new Paragraph("Miqdar /\nQuantity", arial9));
                setCellBeslMainHeader(cell);
                table.AddCell(cell);

                cell = new PdfPCell(new Paragraph("Vahid qiyməti /\nUnit Price", arial9));
                setCellBeslMainHeader(cell);
                table.AddCell(cell);

                cell = new PdfPCell(new Paragraph("Məbləğ /\nAmount", arial9));
                setCellBeslMainHeader(cell);
                table.AddCell(cell);

                cell = new PdfPCell(new Paragraph("Çatdırılma müddəti /\nDelivery duration", arial9));
                setCellBeslMainHeader(cell);
                table.AddCell(cell);

                cell = new PdfPCell(new Paragraph("Qeydlər /\nComments", arial9));
                setCellBeslMainHeader(cell);
                table.AddCell(cell);

                int i = 1;
                foreach (var item in offerData.ItemList)
                {
                    cell = new PdfPCell(new Paragraph(i + "", arial8));
                    setCellBeslMainBody(cell);
                    cell.PaddingTop = 7;
                    table.AddCell(cell);

                    string a = item.Description;
                    string b = "OUR OFFER: ";
                    string c = item.DescriptionForSale;
                    phrase = new Phrase(11, a + "\n", arial8);
                    cell = new PdfPCell();
                    cell.AddElement(phrase);
                    cell.AddElement(new Phrase(0, " ", arial1));
                    phrase = new Phrase(11, b + c, arial8);
                    cell.AddElement(phrase);
                    setCellBeslMainBody(cell);
                    cell.HorizontalAlignment = Element.ALIGN_LEFT;
                    cell.PaddingBottom = 6;
                    cell.PaddingTop = 4;
                    table.AddCell(cell);

                    cell = new PdfPCell(new Paragraph(item.Unit, arial8));
                    setCellBeslMainBody(cell);
                    table.AddCell(cell);

                    cell = new PdfPCell(new Paragraph(item.Quantity + "", arial8));
                    setCellBeslMainBody(cell);
                    table.AddCell(cell);

                    cell = new PdfPCell(new Paragraph(item.UnitPrice + "", arial8));
                    setCellBeslMainBody(cell);
                    table.AddCell(cell);

                    cell = new PdfPCell(new Paragraph(item.Amount + "", arial8));
                    setCellBeslMainBody(cell);
                    table.AddCell(cell);

                    cell = new PdfPCell(new Paragraph(item.DeliveryDuration, arial8));
                    setCellBeslMainBody(cell);
                    table.AddCell(cell);

                    cell = new PdfPCell(new Paragraph(item.Comments, arial8));
                    setCellBeslMainBody(cell);
                    table.AddCell(cell);

                    i++;
                }
                doc.Add(table);

                float currentYPos = writer.GetVerticalPosition(true);
                float pageHeight = doc.PageSize.Height;
                float bottomPadding = doc.BottomMargin;

                if (currentYPos - bottomPadding < 100)
                {
                    doc.NewPage();
                }

                table = new PdfPTable(new float[] { 60, 780, 120, 140, 190, 180, 270, 270 });
                table.TotalWidth = 755;
                table.LockedWidth = true;
                table.HorizontalAlignment = Element.ALIGN_CENTER;

                cell = new PdfPCell(new Paragraph("Total / Cəm", arial9));
                cell.Colspan = 5;
                setCellBeslMainHeader(cell);
                table.AddCell(cell);

                cell = new PdfPCell(new Paragraph("" + offerData.TotalAmount, arial9));
                setCellBeslMainHeader(cell);
                table.AddCell(cell);

                cell = new PdfPCell(new Paragraph(" ", arial9));
                setCellBeslMainHeader(cell);
                table.AddCell(cell);

                cell = new PdfPCell(new Paragraph(" ", arial9));
                setCellBeslMainHeader(cell);
                table.AddCell(cell);

                cell = new PdfPCell(new Paragraph("VAT / ƏDV " + (int)offerData.VAT + "%", arial9));
                cell.Colspan = 5;
                setCellBeslMainHeader(cell);
                table.AddCell(cell);

                cell = new PdfPCell(new Paragraph("" + offerData.VATAmount, arial9));
                setCellBeslMainHeader(cell);
                table.AddCell(cell);

                cell = new PdfPCell(new Paragraph(" ", arial9));
                setCellBeslMainHeader(cell);
                table.AddCell(cell);

                cell = new PdfPCell(new Paragraph(" ", arial9));
                setCellBeslMainHeader(cell);
                table.AddCell(cell);

                cell = new PdfPCell(new Paragraph("Quotation Total / Toplam", arial9));
                cell.Colspan = 5;
                setCellBeslMainHeader(cell);
                table.AddCell(cell);

                cell = new PdfPCell(new Paragraph("" + offerData.QuotationTotal, arial9));
                setCellBeslMainHeader(cell);
                table.AddCell(cell);

                cell = new PdfPCell(new Paragraph(offerData.Currency, arial9));
                setCellBeslMainHeader(cell);
                table.AddCell(cell);

                cell = new PdfPCell(new Paragraph(" ", arial9));
                setCellBeslMainHeader(cell);
                table.AddCell(cell);

                doc.Add(table);
                doc.Add(slashNPara);

                #endregion

                #region footer

                tableOuter = new PdfPTable(1);
                tableOuter.TotalWidth = 515;
                tableOuter.LockedWidth = true;
                tableOuter.HorizontalAlignment = Element.ALIGN_LEFT;

                table = new PdfPTable(1);
                table.TotalWidth = 400;
                table.LockedWidth = true;
                table.HorizontalAlignment = Element.ALIGN_LEFT;

                cell = new PdfPCell(new Paragraph(" Mühasibatlıq / Accounting : accountant @bakuengineering.com", arial9));
                setCellBeslFooter(cell);
                cell.Padding = 5;
                table.AddCell(cell);

                cell = new PdfPCell(new Paragraph(" Satış departamenti / Sales Department: sales @bakuengineering.com", arial9));
                setCellBeslFooter(cell);
                cell.Padding = 5;
                table.AddCell(cell);

                cell = new PdfPCell(new Paragraph(" Alış departamenti / Procurement Department: procurement @bakuengineering.com", arial9));
                setCellBeslFooter(cell);
                cell.Padding = 5;
                table.AddCell(cell);

                cell = new PdfPCell(new Paragraph(" Təhvil Yeri / Terms of delivery : " + offerData.TermsOfDelivery, arial9));
                setCellBeslFooter(cell);
                cell.Padding = 5;
                table.AddCell(cell);

                cell = new PdfPCell(new Paragraph(" Ödəmə Vaxtı / Terms of payment : " + offerData.TermsOfPayment, arial9));
                setCellBeslFooter(cell);
                cell.Padding = 5;
                table.AddCell(cell);

                cell = new PdfPCell();
                cell.Border = Rectangle.NO_BORDER;
                cell.Padding = 20;
                cell.PaddingBottom = 5;
                cell.AddElement(table);
                tableOuter.AddCell(cell);

                doc.Add(tableOuter);

                #endregion

                doc.NewPage();

                doc.Close();
                return new JsonResultMainReturn(true, "Success");
            }
            catch (Exception ex)
            {
                return new JsonResultMainReturn(false, ex.Message);
            }
        }

        public PrintOffer getOfferData(int offerId)
        {
            return new BeslOfferController().getPrintOfferData(offerId);
        }

        public string getOfferFileName(int offerId)
        {
            BeslOffer offer = db.BeslOffer.FirstOrDefault(f => f.BeslOfferId == offerId);
            string quoteNo = offer.QuoteNo;
            string inquiryNo = db.Inquiry.FirstOrDefault(f => f.InquiryId == offer.InquiryId).InquiryNo;

            return quoteNo + " " + inquiryNo + ".pdf";
        }

        private void setBorderColorBesl(PdfPCell cell)
        {
            cell.BorderColor = new iTextSharp.text.BaseColor(55, 25, 4);
        }
        private void setHeaderTableCell(PdfPCell cell)
        {
            cell.BorderWidth = 0; ;
        }

        private void setCellBeslMainHeader(PdfPCell cell)
        {
            cell.HorizontalAlignment = Element.ALIGN_CENTER;
            cell.VerticalAlignment = Element.ALIGN_CENTER;
            cell.Padding = 5;
            cell.PaddingBottom = 7;
            setBorderColorBesl(cell);
            setCellColorBeslMainHeader(cell);
        }
        private void setCellBeslMainBody(PdfPCell cell)
        {
            cell.HorizontalAlignment = Element.ALIGN_CENTER;
            cell.VerticalAlignment = Element.ALIGN_CENTER;
            cell.Padding = 4;
            cell.PaddingTop = 10;
        }

        private void setCellColorBeslMainHeader(PdfPCell cell)
        {
            cell.BackgroundColor = new iTextSharp.text.BaseColor(255, 170, 150);
        }

        private void setCellBeslFooter(PdfPCell cell)
        {
            setCellColorBeslFooter(cell);
            RoundRectangle rr = new RoundRectangle();
            //cell.CellEvent = rr;
            cell.BorderWidth = 0;
        }

        private void setCellColorBeslFooter(PdfPCell cell)
        {
            cell.BackgroundColor = new iTextSharp.text.BaseColor(255, 210, 190);
        }

        public class RoundRectangle : IPdfPCellEvent
        {
            public void CellLayout(
              PdfPCell cell, Rectangle rect, PdfContentByte[] canvas
            )
            {
                PdfContentByte cb = canvas[PdfPTable.LINECANVAS];
                cb.RoundRectangle(
                  rect.Left,
                  rect.Bottom,
                  rect.Width,
                  rect.Height,
                  4 // change to adjust how "round" corner is displayed
                );
                cb.SetLineWidth(1f);
                cb.SetCMYKColorStrokeF(0f, 0f, 0f, 1f);
                cb.Stroke();
            }
        }
    }
}