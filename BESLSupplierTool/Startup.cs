﻿using Microsoft.Owin;
using Owin;

[assembly: OwinStartupAttribute(typeof(BESLSupplierTool.Startup))]
namespace BESLSupplierTool
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            ConfigureAuth(app);
        }
    }
}
