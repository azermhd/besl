﻿namespace BESLSupplierTool.Models.ReturnModels
{
    public class JsonResultMainReturn
    {
        public JsonResultMainReturn()
        {
        }

        public JsonResultMainReturn(bool Success, string Message)
        {
            this.Success = Success;
            this.Message = Message;
        }

        public bool Success { get; set; }
        public string Message { get; set; }
    }
}