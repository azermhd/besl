﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Web;

namespace BESLSupplierTool.Models.DBModels
{
    public class DatabaseContext : DbContext
    {
        public DatabaseContext()
            : base("name=BESLConnection")
        {
        }

        public virtual DbSet<Inquiry> Inquiry { get; set; }
        public virtual DbSet<InquiryItem> InquiryItem { get; set; }
        public virtual DbSet<BeslOffer> BeslOffer { get; set; }
        public virtual DbSet<BeslOfferItem> BeslOfferItem { get; set; }
        public virtual DbSet<CompanyDetails> CompanyDetails { get; set; }
    }
}