﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace BESLSupplierTool.Models.DBModels
{
    public class CompanyDetails
    {
        [Key]
        public int CompanyId { get; set; }

        [Required(ErrorMessage = "Required")]
        public string CompanyName { get; set; }

        [Required(ErrorMessage = "Required")]
        public string OfferPrefix { get; set; }

        public int OfferNoLength { get; set; }
    }
}