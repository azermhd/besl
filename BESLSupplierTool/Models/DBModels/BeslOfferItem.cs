﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace BESLSupplierTool.Models.DBModels
{
    public class BeslOfferItem
    {
        public BeslOfferItem()
        {
        }

        public BeslOfferItem(BeslOfferItemView beslOfferItemView)
        {
            this.BeslOfferItemId = beslOfferItemView.BeslOfferItemId;
            this.BeslOfferId = beslOfferItemView.BeslOfferId;
            this.Description = beslOfferItemView.Description;
            this.DescriptionForSale = beslOfferItemView.DescriptionForSale;
            this.Unit = beslOfferItemView.Unit;
            this.Quantity = beslOfferItemView.Quantity;
            this.UnitPrice = beslOfferItemView.UnitPrice;
            this.Amount = beslOfferItemView.Amount;
            this.DeliveryDuration = beslOfferItemView.DeliveryDuration;
            this.Comments = beslOfferItemView.Comments;
        }

        [Key]
        [Display(Name = "BeslOfferItem")]
        public int BeslOfferItemId { get; set; }

        [ForeignKey("BeslOffer")]
        [Display(Name = "BeslOffer")]
        public int BeslOfferId { get; set; }

        [Display(Name = "Description")]
        public string Description { get; set; }

        [Display(Name = "DescriptionForSale")]
        public string DescriptionForSale { get; set; }

        [Display(Name = "Unit")]
        public string Unit { get; set; }

        [Display(Name = "Quantity")]
        [Required(ErrorMessage="Required")]
        public decimal Quantity { get; set; }

        [Display(Name = "UnitPrice")]
        [Required(ErrorMessage = "Required")]
        public decimal UnitPrice { get; set; }

        [Display(Name = "Amount")]
        [Required(ErrorMessage = "Required")]
        public decimal Amount { get; set; }

        [Display(Name = "DeliveryDuration")]
        public string DeliveryDuration { get; set; }

        [Display(Name = "Comments")]
        public string Comments { get; set; }

        public virtual BeslOffer BeslOffer { get; set; }
    }
}