﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace BESLSupplierTool.Models.DBModels
{
    public class InquiryItem
    {
        public InquiryItem()
        {
        }
        public InquiryItem(InquiryItemView inquiryItemView)
        {
            this.InquiryItemId = inquiryItemView.InquiryItemId;
            this.InquiryId = inquiryItemView.InquiryId;
            this.Description = inquiryItemView.Description;
            this.Unit = inquiryItemView.Unit;
            this.Quantity = inquiryItemView.Quantity;
        }

        [Key]
        [Display(Name = "InquiryItem")]
        public int InquiryItemId { get; set; }

        [ForeignKey("Inquiry")]
        [Display(Name = "Inquiry")]
        public int InquiryId { get; set; }

        [Display(Name = "Description")]
        [Required(ErrorMessage = "Required")]
        public string Description { get; set; }

        [Display(Name = "Unit")]
        [Required(ErrorMessage = "Required")]
        public string Unit { get; set; }

        [Display(Name = "Quantity")]
        [Required(ErrorMessage = "Required")]
        public decimal Quantity { get; set; }

        public virtual Inquiry Inquiry { get; set; }
    }
}