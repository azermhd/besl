﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace BESLSupplierTool.Models.DBModels
{
    public class BeslOffer
    {
        [Key]
        [Display(Name = "Offer")]
        public int BeslOfferId { get; set; }

        [ForeignKey("Inquiry")]
        [Display(Name = "Inquiry")]
        public int InquiryId { get; set; }

        [NotMapped]
        [Display(Name = "Inquiry")]
        public string InquiryNo { get; set; }

        [Display(Name = "QuoteNo")]
        [Required(ErrorMessage = "Required")]
        public string QuoteNo { get; set; }

        [Display(Name = "Date")]
        [DataType(DataType.Date)]
        [Required(ErrorMessage = "Required")]
        public DateTime DateOffer { get; set; }

        [Display(Name = "Currency")]
        [Required(ErrorMessage = "Required")]
        public string Currency { get; set; }

        [Display(Name = "VAT")]
        [Required(ErrorMessage = "Required")]
        public decimal VAT { get; set; }

        [Display(Name = "TermsOfDelivery")]
        [Required(ErrorMessage = "Required")]
        public string TermsOfDelivery { get; set; }

        [Display(Name = "TermsOfPayment")]
        [Required(ErrorMessage = "Required")]
        public string TermsOfPayment { get; set; }

        public virtual Inquiry Inquiry { get; set; }
    }
}