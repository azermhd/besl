﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace BESLSupplierTool.Models.DBModels
{
    public class Inquiry
    {
        [Key]
        [Display(Name = "Inquiry")]
        public int InquiryId { get; set; }

        [Display(Name = "To")]
        [Required(ErrorMessage = "Required")]
        public string To { get; set; }

        [Display(Name = "Buyer")]
        [Required(ErrorMessage = "Required")]
        public string Buyer { get; set; }

        [Display(Name = "Attn")]
        [Required(ErrorMessage = "Required")]
        public string Attn { get; set; }

        [Display(Name = "InquiryNo")]
        [Required(ErrorMessage = "Required")]
        public string InquiryNo { get; set; }

        [Display(Name = "ProjectNo")]
        [Required(ErrorMessage = "Required")]
        public string ProjectNo { get; set; }
    }
}