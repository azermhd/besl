﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace BESLSupplierTool.Models.ViewModels
{
    public class OfferItemRowInEditTool
    {
        public string description { get; set; }
        public string descriptionforsale { get; set; }
        public string unit { get; set; }
        public string quantity { get; set; }
        public string unitprice { get; set; }
        public string amount { get; set; }
        public string deliveryduration { get; set; }
        public string comments { get; set; }
    }
}