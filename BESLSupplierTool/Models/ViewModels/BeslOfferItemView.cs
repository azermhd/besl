﻿using System.ComponentModel.DataAnnotations;

namespace BESLSupplierTool.Models.DBModels
{
    public class BeslOfferItemView
    {
        public BeslOfferItemView()
        {
        }

        public BeslOfferItemView(BeslOfferItem beslOfferItem)
        {
            this.BeslOfferItemId = beslOfferItem.BeslOfferItemId;
            this.BeslOfferId = beslOfferItem.BeslOfferId;
            this.Description = beslOfferItem.Description;
            this.DescriptionForSale = beslOfferItem.DescriptionForSale;
            this.Unit = beslOfferItem.Unit;
            this.Quantity = beslOfferItem.Quantity;
            this.UnitPrice = beslOfferItem.UnitPrice;
            this.Amount = beslOfferItem.Amount;
            this.DeliveryDuration = beslOfferItem.DeliveryDuration;
            this.Comments = beslOfferItem.Comments;
        }

        [Key]
        [Display(Name = "BeslOfferItem")]
        public int BeslOfferItemId { get; set; }

        [Display(Name = "BeslOffer")]
        public int BeslOfferId { get; set; }

        [Display(Name = "Description")]
        public string Description { get; set; }

        [Display(Name = "DescriptionForSale")]
        public string DescriptionForSale { get; set; }

        [Display(Name = "Unit")]
        public string Unit { get; set; }

        [Display(Name = "Quantity")]
        [Required(ErrorMessage = "Required")]
        public decimal Quantity { get; set; }

        [Display(Name = "UnitPrice")]
        [Required(ErrorMessage = "Required")]
        public decimal UnitPrice { get; set; }

        [Display(Name = "Amount")]
        [Required(ErrorMessage = "Required")]
        public decimal Amount { get; set; }

        [Display(Name = "DeliveryDuration")]
        public string DeliveryDuration { get; set; }

        [Display(Name = "Comments")]
        public string Comments { get; set; }
    }

}