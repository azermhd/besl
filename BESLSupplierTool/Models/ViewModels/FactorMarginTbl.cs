﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace BESLSupplierTool.Models.ViewModels
{
    public class FactorMarginTbl
    {
        public string Factor { get; set; }
        public string Margin { get; set; }
        public string Amount { get; set; }
        public string PreFactor { get; set; }
        public string PreMargin { get; set; }
        public string PreAmount { get; set; }
    }
}