﻿
namespace BESLSupplierTool.Models.ViewModels
{
    public class InquiryItemRowInEditTool
    {
        public string description { get; set; }
        public string unit { get; set; }
        public string quantity { get; set; }
    }
}