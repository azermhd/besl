﻿using BESLSupplierTool.Models.DBModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace BESLSupplierTool.Models.ViewModels
{
    public class EditOfferView
    {
        public BeslOfferItem UnusedOfferItem { get; }
        public BeslOfferItem[] OfferItems { get; set; }
        public BeslOffer Offer { get; set; }
        public string EnquiryQuoteNo { get; set; }
        public FactorMarginTbl FactorMarginTbl { get; set; }
    }
}