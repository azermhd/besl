﻿using BESLSupplierTool.Models.DBModels;

namespace BESLSupplierTool.Models.ViewModels
{
    public class EditInquiryView
    {
        public InquiryItem UnusedInquiryItem { get; }
        public string InquiryNo { get; set; }
        public int InquiryId { get; set; }
        public InquiryItemView[] InquiryItems { get; set; }
    }
}