﻿using BESLSupplierTool.Models.DBModels;

namespace BESLSupplierTool.Models.ViewModels
{
    public class PrintInquiry
    {
        public string Date { get; set; }
        public string InquiryNo { get; set; }
        public string ProjectNo { get; set; }
        public InquiryItemView[] ItemList { get; set; }
    }
}