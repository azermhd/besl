﻿using System.ComponentModel.DataAnnotations;

namespace BESLSupplierTool.Models.DBModels
{
    public class InquiryItemView
    {
        public InquiryItemView()
        {
        }
        public InquiryItemView(InquiryItem inquiryItem)
        {
            this.InquiryItemId = inquiryItem.InquiryItemId;
            this.InquiryId = inquiryItem.InquiryId;
            this.Description = inquiryItem.Description;
            this.Unit = inquiryItem.Unit;
            this.Quantity = inquiryItem.Quantity;
        }

        [Key]
        [Display(Name = "InquiryItem")]
        public int InquiryItemId { get; set; }

        [Display(Name = "Inquiry")]
        public int InquiryId { get; set; }

        [Display(Name = "Description")]
        [Required(ErrorMessage = "Required")]
        public string Description { get; set; }

        [Display(Name = "Unit")]
        [Required(ErrorMessage = "Required")]
        public string Unit { get; set; }

        [Display(Name = "Quantity")]
        [Required(ErrorMessage = "Required")]
        public decimal Quantity { get; set; }

    }
}