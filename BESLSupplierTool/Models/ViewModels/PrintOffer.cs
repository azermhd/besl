﻿using BESLSupplierTool.Models.DBModels;

namespace BESLSupplierTool.Models.ViewModels
{
    public class PrintOffer
    {
        public string To { get; set; }
        public string Buyer { get; set; }
        public string Attn { get; set; }
        public string Date { get; set; }
        public string InquiryNo { get; set; }
        public string ProjectNo { get; set; }
        public string QuoteNo { get; set; }
        public string TermsOfDelivery { get; set; }
        public string TermsOfPayment { get; set; }
        public BeslOfferItemView[] ItemList { get; set; }
        public decimal TotalAmount { get; set; }
        public decimal VAT { get; set; }
        public decimal VATAmount { get; set; }
        public decimal QuotationTotal { get; set; }
        public string Currency { get; set; }
    }
}