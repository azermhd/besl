// <auto-generated />
namespace BESLSupplierTool.Migrations
{
    using System.CodeDom.Compiler;
    using System.Data.Entity.Migrations;
    using System.Data.Entity.Migrations.Infrastructure;
    using System.Resources;
    
    [GeneratedCode("EntityFramework.Migrations", "6.1.3-40302")]
    public sealed partial class termsofdeliverypayment : IMigrationMetadata
    {
        private readonly ResourceManager Resources = new ResourceManager(typeof(termsofdeliverypayment));
        
        string IMigrationMetadata.Id
        {
            get { return "201709131048340_termsofdeliverypayment"; }
        }
        
        string IMigrationMetadata.Source
        {
            get { return null; }
        }
        
        string IMigrationMetadata.Target
        {
            get { return Resources.GetString("Target"); }
        }
    }
}
