namespace BESLSupplierTool.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class companyDetailsAddOffernoLenght : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.CompanyDetails", "OfferNoLength", c => c.Int(nullable: false));
        }
        
        public override void Down()
        {
            DropColumn("dbo.CompanyDetails", "OfferNoLength");
        }
    }
}
