namespace BESLSupplierTool.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class termsofdeliverypayment : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.BeslOffers", "TermsOfDelivery", c => c.String(nullable: false));
            AddColumn("dbo.BeslOffers", "TermsOfPayment", c => c.String(nullable: false));
        }
        
        public override void Down()
        {
            DropColumn("dbo.BeslOffers", "TermsOfPayment");
            DropColumn("dbo.BeslOffers", "TermsOfDelivery");
        }
    }
}
