namespace BESLSupplierTool.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class datenamechange : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.BeslOffers", "DateOffer", c => c.DateTime(nullable: false));
            DropColumn("dbo.BeslOffers", "Date");
        }
        
        public override void Down()
        {
            AddColumn("dbo.BeslOffers", "Date", c => c.DateTime(nullable: false));
            DropColumn("dbo.BeslOffers", "DateOffer");
        }
    }
}
