namespace BESLSupplierTool.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class helloworld : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.BeslOffers",
                c => new
                    {
                        BeslOfferId = c.Int(nullable: false, identity: true),
                        InquiryId = c.Int(nullable: false),
                        QuoteNo = c.String(nullable: false),
                        Date = c.DateTime(nullable: false),
                        Currency = c.String(nullable: false),
                        VAT = c.Decimal(nullable: false, precision: 18, scale: 2),
                    })
                .PrimaryKey(t => t.BeslOfferId)
                .ForeignKey("dbo.Inquiries", t => t.InquiryId, cascadeDelete: true)
                .Index(t => t.InquiryId);
            
            CreateTable(
                "dbo.Inquiries",
                c => new
                    {
                        InquiryId = c.Int(nullable: false, identity: true),
                        To = c.String(nullable: false),
                        Buyer = c.String(nullable: false),
                        Attn = c.String(nullable: false),
                        InquiryNo = c.String(nullable: false),
                        ProjectNo = c.String(nullable: false),
                    })
                .PrimaryKey(t => t.InquiryId);
            
            CreateTable(
                "dbo.BeslOfferItems",
                c => new
                    {
                        BeslOfferItemId = c.Int(nullable: false, identity: true),
                        BeslOfferId = c.Int(nullable: false),
                        Description = c.String(),
                        DescriptionForSale = c.String(),
                        Unit = c.String(),
                        Quantity = c.Decimal(nullable: false, precision: 18, scale: 2),
                        UnitPrice = c.Decimal(nullable: false, precision: 18, scale: 2),
                        Amount = c.Decimal(nullable: false, precision: 18, scale: 2),
                        DeliveryDuration = c.String(),
                        Comments = c.String(),
                    })
                .PrimaryKey(t => t.BeslOfferItemId)
                .ForeignKey("dbo.BeslOffers", t => t.BeslOfferId, cascadeDelete: true)
                .Index(t => t.BeslOfferId);
            
            CreateTable(
                "dbo.InquiryItems",
                c => new
                    {
                        InquiryItemId = c.Int(nullable: false, identity: true),
                        InquiryId = c.Int(nullable: false),
                        Description = c.String(nullable: false),
                        Unit = c.String(nullable: false),
                        Quantity = c.Decimal(nullable: false, precision: 18, scale: 2),
                    })
                .PrimaryKey(t => t.InquiryItemId)
                .ForeignKey("dbo.Inquiries", t => t.InquiryId, cascadeDelete: true)
                .Index(t => t.InquiryId);
            
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.InquiryItems", "InquiryId", "dbo.Inquiries");
            DropForeignKey("dbo.BeslOfferItems", "BeslOfferId", "dbo.BeslOffers");
            DropForeignKey("dbo.BeslOffers", "InquiryId", "dbo.Inquiries");
            DropIndex("dbo.InquiryItems", new[] { "InquiryId" });
            DropIndex("dbo.BeslOfferItems", new[] { "BeslOfferId" });
            DropIndex("dbo.BeslOffers", new[] { "InquiryId" });
            DropTable("dbo.InquiryItems");
            DropTable("dbo.BeslOfferItems");
            DropTable("dbo.Inquiries");
            DropTable("dbo.BeslOffers");
        }
    }
}
