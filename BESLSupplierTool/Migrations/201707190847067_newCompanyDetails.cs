namespace BESLSupplierTool.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class newCompanyDetails : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.CompanyDetails",
                c => new
                    {
                        CompanyId = c.Int(nullable: false, identity: true),
                        CompanyName = c.String(nullable: false),
                        OfferPrefix = c.String(nullable: false),
                    })
                .PrimaryKey(t => t.CompanyId);
            
        }
        
        public override void Down()
        {
            DropTable("dbo.CompanyDetails");
        }
    }
}
